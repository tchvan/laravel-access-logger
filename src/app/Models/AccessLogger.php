<?php

namespace Fortunetr\AccessLogger\app\Models;

use Illuminate\Database\Eloquent\Model;

class AccessLogger extends Model
{
    public $timestamps = false; //Ignore "Created_at", "Updated_at" column

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ["id", "created_at", "updated_at"];
}
