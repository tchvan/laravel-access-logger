<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function getConnection(): string|null
    {
        return config('access_logger.storage.database.connection');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $schema = Schema::connection($this->getConnection());
        $schema->create('access_loggers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('owner_id')->nullable();
            $table->string('route_name');
            $table->string('entity_name');
            $table->string('entity_id')->nullable();
            $table->string('env');
            $table->unsignedInteger('took');
            $table->float('memory_in_mb');
            $table->string('url');

            $table->timestamp('created_at')->useCurrent();

            $table->index('env');
            $table->index('url');
            $table->index('route_name');
            $table->index('took');
            $table->index('entity_name');
            $table->index('entity_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schema = Schema::connection($this->getConnection());
        $schema->dropIfExists('access_loggers');
    }
};
